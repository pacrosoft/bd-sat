<?php
require_once 'controller/conexion.php';

function getListasOrigen(){
  $mysqli = getConn();
  $query = "SELECT * FROM `origen` ";
  $result = $mysqli->query($query);
  $listas = '<option value="0">Elige una opción</option>';
  while($row = $result->fetch_array(MYSQLI_ASSOC)){
    $listas .= "<option value='$row[origen_id]'>$row[origen_name]</option>";
  }
  return $listas;
}

echo getListasOrigen();
