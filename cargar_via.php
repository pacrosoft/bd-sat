<?php
require_once 'controller/conexion.php';

function getListasVia(){


  $mysqli = getConn();
  $carga_via="";
  if(isset($_REQUEST['via']))
  {
      $carga_via=$_REQUEST['via'];
  }


  $query = "SELECT * FROM `via` ";


  $via = $mysqli->query($query);
  $listas = '<option value="0">Elige una opción</option>';
  while($row = $via->fetch_array(MYSQLI_ASSOC))
  {
      if($carga_via==$row['via_id'])
      {
          $listas.="<option value='".$row['via_id']."' selected>".$row['via_nombre']."</option>";
      }
      else
      {
          $listas.="<option value='".$row['via_id']."'>".$row['via_nombre']."</option>";
      }
  }


return $listas;
}

echo getListasVia();
