<!DOCTYPE html>
<?php
include "autoload.php";
include "controller/StaticData.php";
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BD-SAT | Boooking</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- color picker -->
  <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/NelsonSudio.min.css">


</head>
<body class="hold-transition skin-green layout-top-nav">
<div class="wrapper">

  <header class="main-header">

    <nav class="navbar navbar-static-top" style="background-color: #0001a6;">



           <center><h2 style="color: white;">
          <span class="logo-lg"><b>BD-SAT</b></span>

</h2></center>


    </nav>
  </header>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-calendar"></i> Booking
        <small>Ver 1.0</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-body no-padding">
              <!-- Booking-->
              <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
</div>
  <!-- Modal add. update, delete-->
  <div class="modal fade" id="ModalEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h2 class="modal-title" id="titleEvent"></h2>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnClose1">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-offset-1 col-md-10">

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon">Evento:</span>
                      <input type="text" class="form-control" name="txtTitle" id="txtTitle" required placeholder="Nombre del Evento">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon">Origen:</span>
                      <select id="cargar_origen" name="cargar_origen" class="form-control">
                      </select>
                  </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon">CORTESIA:</span>
                      <select id="cargar_canales" name="cargar_canales" class="form-control">
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon">VIA:</span>
                      <select id="cargar_via" name="cargar_via" class="form-control">
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon">Observaciones</span>
                      <textarea class="form-control" name="txtDescription" id="txtDescription"></textarea>
                    </div>
                  </div>



                  <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"> Fecha Inicio: </span>
                        <input type="date" class="form-control" name="txtDate" id="txtDate" required >
                        <span class="input-group-addon"> Tiempo Inicio: </span>
                        <input type="time" class="form-control" name="txtHour" id="txtHour"  required >
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"> Fecha Final : </span>
                        <input type="date" class="form-control" name="txtDateEnd" id="txtDateEnd"  required >
                        <span class="input-group-addon"> Tiempo Final : </span>
                        <input type="time" class="form-control" name="txtHourEnd" id="txtHourEnd" required >

                    </div>
                  </div>





                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon">Color</span>
                      <div class="input-group my-colorpicker2">
                      <input type="text" name="txtColor" id="txtColor" value="#00a65a"  class="form-control">

                      <div class="input-group-addon">
                        <i></i>
                      </div>
                    </div>

                    </div>
                  </div>


                </div>
               </div>

              <input type="hidden" id="txtId" name="txtId"><br>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success"  id="btnAdd">Agregar</button>
              <button type="button" class="btn btn-secondary" id="btnUpdate">Actualizar</button>
              <button type="button" class="btn btn-danger" id="btnDel">Borrar</button>
              <button type="button" class="btn btn-default"  id="btnClose">Cancelar</button>

            </div>
          </div>
        </div>
  </div>


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Centro de Control Satelital &copy; 2019 .</strong>
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- Color picker -->
<script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- NelsonSudio App -->
<script src="dist/js/app.min.js"></script>
<!-- NelsonSudio for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Page specific script -->
<script>

  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 1070,
          revert: true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        });

      });
    }

    ini_events($('#external-events div.external-event'));

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week: 'week',
        day: 'day'
      },
      //Random default events
      events:"event.php",
      editable: true,
      selectable:true,
      eventLimit: true,
      droppable: true, // this allows things to be dropped onto the calendar !!!
      drop: function (date, allDay) { // this function is called when something is dropped


        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);

        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        copiedEventObject.backgroundColor = $(this).css("background-color");
        copiedEventObject.borderColor = $(this).css("border-color");


        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)


        $('#txtTitle').val(copiedEventObject.title);
        $('#txtDate').val(copiedEventObject.start.format());
        $('#txtDateEnd').val(copiedEventObject.start.format());
        $('#txtDescription').val(copiedEventObject.backgroundColor);
        DataGUI();
        DataSendUI('agregarDefault',NewEvent);

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();

          alert(nuevo);
        };



      },

      eventResize:function(calEvent, delta, revertFunc){

        $('#txtId').val(calEvent.id);
            $('#txtTitle').val(calEvent.title);
            $('#txtColor').val(calEvent.color);
            $('#txtDescription').val(calEvent.description);
            $('#cargar_origen').val(calEvent.cargar_origen);
            $('#cargar_canales').val(calEvent.cargar_canales);
            $('#cargar_via').val(calEvent.cargar_canales);

            var fechaHora= calEvent.start.format().split("T");
            var fechaHoraEnd= calEvent.end.format().split("T");
            $('#txtDate').val(fechaHora[0]);
            $('#txtHour').val(fechaHora[1]);

            $('#txtDateEnd').val(fechaHoraEnd[0]);
            $('#txtHourEnd').val(fechaHoraEnd[1]);

            DataGUI();
            DataSendUI('actualizar',NewEvent,true);





      },
      eventClick:function(calEvent){
            var canal_origen=calEvent.cargar_origen;
            var cargar_canal=calEvent.cargar_canales;
            var cargar_via=calEvent.cargar_via;
            
            $.ajax({
              type: 'POST',
              url: 'cargar_canales.php',
              data: {'id': canal_origen,'canal':cargar_canal}

            })
            .done(function(listas_rep){
              $('#cargar_canales').html(listas_rep);
            })
            .fail(function(){
              alert('Hubo un errror al cargar los canales')
            })

            $.ajax({
              type: 'POST',
              url: 'cargar_via.php',
              data: {'via':cargar_via}

            })
            .done(function(listas_rep){
              $('#cargar_via').html(listas_rep);
            })
            .fail(function(){
              alert('Hubo un errror al cargar las vias')
            })

            // H2
            $('#titleEvent').html(calEvent.title);
            // Information events
            $('#txtDescription').val(calEvent.description);
            $('#cargar_origen').val(calEvent.cargar_origen);
            //$('#cargar_via').val(calEvent.cargar_via);
            //$('#cargar_canales').val(calEvent.cargar_canales);
            $('#txtId').val(calEvent.id);
            $('#txtTitle').val(calEvent.title);
            $('#txtColor').val(calEvent.color);
            $('#txtColorHidden').val(calEvent.color);

            datehhour= calEvent.start._i.split(" ");
            datehhourEnd= calEvent.end._i.split(" ");
            $('#txtDate').val(datehhour[0]);
            $('#txtHour').val(datehhour[1]);
            $('#txtDateEnd').val(datehhourEnd[0]);
            $('#txtHourEnd').val(datehhourEnd[1]);

            $("#ModalEvent").modal();




      },
      dayClick:function(date,jsEvent,view){
          $('#txtDate').val(date.format());
          $('#txtDateEnd').val(date.format());
        //  $("#ModalEvent").modal();

      },
      select: function(startDate, endDate) {


        var fechaHora=startDate.format().split("T");
        var fechaHoraEnd=endDate.format().split("T");
        $('#txtDate').val(fechaHora[0]);
        $('#txtHour').val(fechaHora[1]);

        $('#txtDateEnd').val(fechaHoraEnd[0]);
        $('#txtHourEnd').val(fechaHoraEnd[1]);

        $.ajax({
          type: 'POST',
          url: 'cargar_via.php'
        })
        .done(function(listas_rep){
          $('#cargar_via').html(listas_rep);
        })
        .fail(function(){
          alert('Hubo un errror al cargar las vias')
        })

        $("#ModalEvent").modal();

    },

      eventDrop:function(calEvent){

          var canal_origen=calEvent.cargar_origen;
          var cargar_canal=calEvent.cargar_canales;
          var cargar_via=calEvent.cargar_via;

          $.ajax({
            type: 'POST',
            url: 'cargar_canales.php',
            data: {'id': canal_origen,'canal':cargar_canal}

          })
          .done(function(listas_rep){
            $('#cargar_canales').html(listas_rep);
            $.ajax({
              type: 'POST',
              url: 'cargar_via.php',
              data: {'via':cargar_via}

            })
            .done(function(listas_rep){
              $('#cargar_via').html(listas_rep);
              $('#txtId').val(calEvent.id);
              $('#titleEvent').val(calEvent.title);
              $('#txtTitle').val(calEvent.title);
              $('#txtColor').val(calEvent.color);
              $('#txtDescription').val(calEvent.description);
              $('#cargar_origen').val(calEvent.cargar_origen);
              //$('#cargar_canales').val(calEvent.cargar_canales);
              //$('#cargar_via').val(calEvent.cargar_via);
              $('#txtColorHidden').val(calEvent.color);

              var fechaHora=calEvent.start.format().split("T");
              $('#txtDate').val(fechaHora[0]);
              $('#txtHour').val(fechaHora[1]);

              var fechaHoraEnd=calEvent.end.format().split("T");
              $('#txtDateEnd').val(fechaHoraEnd[0]);
              $('#txtHourEnd').val(fechaHoraEnd[1]);

              DataGUI();
              DataSendUI('actualizar',NewEvent,true);
            })
            .fail(function(){
              alert('Hubo un errror al cargar las vias')
            })
          })
          .fail(function(){
            alert('Hubo un errror al cargar los canales')
          })




      }


    });


  });
</script>


<script type="text/javascript">
    var NewEvent;


    /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function (e) {
      e.preventDefault();
      //Save color
      currColor = $(this).css("color");
      //Add color effect to button
      $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
    });



    $("#add-new-event").click(function (e) {

      //Get value and make sure it is not null


      var val = $("#new-event").val();
      if (val.length == 0) {
        alert("Ingresa title");
        return;

    }else{
      DataGUI();
      DataSendUI('addstatic',NewEvent);
      //Create events
      $.ajax({
        url: "delevent.php",
        beforeSend: function(objeto){
        $("#external-events").html("Mensaje: Cargando...");
        },
        success: function(datos){
        $("#external-events").html(datos);
        }
      });
      $("#new-event").val("");
    }


    });


    $('#btnAdd').click(function(){

      DataGUI();
      DataSendUI('agregar',NewEvent);
      $('#ModalEvent').modal('toggle');
      limpiar();
    });

    $('#btnDel').click(function(){

      DataGUI();
      DataSendUI('eliminar',NewEvent);
      $('#ModalEvent').modal('toggle');
      limpiar();
    });

    $('#btnUpdate').click(function(){

      DataGUI();
      DataSendUI('actualizar',NewEvent);
      $('#ModalEvent').modal('toggle');
      limpiar();
    });

    $('#btnClose').click(function(){

      $('#ModalEvent').modal('toggle'),
      limpiar();
    });

    $('#btnClose1').click(function(){
      $('#ModalEvent').modal('toggle'),
      limpiar();
    });

    function limpiar() {
        document.getElementById("txtId").value = "";
        document.getElementById("txtTitle").value = "";
        document.getElementById("txtDescription").value = "";
        document.getElementById("cargar_origen").value ="";
        document.getElementById("cargar_canales").value ="";
        document.getElementById("cargar_via").value = "";
        document.getElementById("txtDate").value = "";
        document.getElementById("txtHour").value = "";
        document.getElementById("txtColor").value = "";

        $("#titleEvent").empty();

    }

    function DataGUI(){

         NewEvent={
        // TABLA STATIC
        s_id:$('#s_txtid').val(),
        s_title:$('#new-event').val(),
        s_color:currColor,
        s_textcolor:"#FFFFFF",
        // TABLE EVENTO
        id:$('#txtId').val(),
        title:$('#txtTitle').val(),
        start:$('#txtDate').val()+" "+$('#txtHour').val(),
        color:$('#txtColor').val(),

        description:$('#txtDescription').val(),
        cargar_origen:$('#cargar_origen').val(),
        cargar_canales:$('#cargar_canales').val(),
        cargar_via:$('#cargar_via').val(),
        textColor:"#FFFFFF",
        end:$('#txtDateEnd').val()+" "+$('#txtHourEnd').val()
      };

    }

    function DataSendUI(accion,objEvento){

        $.ajax({
          type:'POST',
          url:'event.php?accion='+accion,
          data:objEvento,
          success:function(msg){
            if (msg){
              $('#calendar').fullCalendar('refetchEvents');
              if(!modal){
              $('#ModalEvent').modal('toggle');
              }
            }
          },
          error:function(){
            alert("Hay un error");
          }
        });

    }


    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

  </script>


  <script>


      function eliminar (s_id)
    {

      $.ajax({
        type: "GET",
        url: "delevent.php",
        data: "s_id="+s_id,
     beforeSend: function(objeto){
      $("#external-events").html("Mensaje: Cargando...");
      },
        success: function(datos){
    $("#external-events").html(datos);
    }
      });

    }


  </script>
<script type="text/javascript" src="dist/js/index.js"></script>
</body>
</html>
