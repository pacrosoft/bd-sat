<?php
require_once 'controller/conexion.php';

function getListasCanal(){
  $mysqli = getConn();
  $id = $_REQUEST['id'];
  $canal= $_REQUEST['canal'];

  $query = "SELECT * FROM `canal` WHERE origen_id = $id ORDER BY canal_name ASC";
  $result = $mysqli->query($query);
  $videos = '<option value="0">Elige una opción</option>';
  while($row = $result->fetch_array(MYSQLI_ASSOC)){

    if($canal && $canal==$row[canal_id])
    {
        $videos .= "<option value='$row[canal_id]' selected>$row[canal_name]</option>";
    }
    else
    {
        $videos .= "<option value='$row[canal_id]'>$row[canal_name]</option>";
    }
  }

  return $videos;
}

echo getListasCanal();
