-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sistema
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `canal`
--

DROP TABLE IF EXISTS `canal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `canal` (
  `canal_id` int(11) NOT NULL AUTO_INCREMENT,
  `canal_name` varchar(250) NOT NULL,
  `forma_servicio` varchar(50) NOT NULL,
  `origen_id` int(11) NOT NULL,
  PRIMARY KEY (`canal_id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canal`
--

LOCK TABLES `canal` WRITE;
/*!40000 ALTER TABLE `canal` DISABLE KEYS */;
INSERT INTO `canal` VALUES (1,'BEIN Sports ENG','permanente',1),(2,'BEIN Sports ESP','permanente',1),(3,'ESPN2','permanente',1),(4,'TELEMUNDO','permanente',1),(5,'BEIN Sports Connect','permanente',1),(6,'ESPN Deportes','permanente',1),(7,'ESPN Multi Feeds-PLAY)','permanente',1),(8,'NBC Universo','permanente',1),(9,'CBS WKRC-TV Cincinnati','ocasional',1),(10,'CNBC','ocasional',1),(11,'GOLF Channel','ocasional',1),(12,'NBA-TV','ocasional',1),(13,'NBC WLWT-TV Cincinnati','ocasional',1),(14,'RTP','ocasional',1),(15,'TBS','ocasional',1),(16,'CSN PA','ocasional',1),(17,'CSN POR/NBC SPORT NW','ocasional',1),(18,'DAZN','ocasional',1),(19,'FCS Atlantic','ocasional',1),(20,'FCS Central','ocasional',1),(21,'FCS Pacific','ocasional',1),(22,'Fox Sports GO','ocasional',1),(23,'GOL TV','ocasional',1),(24,'MSG','ocasional',1),(25,'MSG PLUS','ocasional',1),(26,'NBC SPORT DIGITAL','ocasional',1),(27,'Networks','ocasional',1),(28,'PAC12 Network','ocasional',1),(29,'TENNIS Channel','ocasional',1),(30,'TSN','ocasional',1),(31,'TyC SPORTS','ocasional',1),(32,'ESPN USA','permanente',2),(33,'ESPN DOS-MEX','permanente',2),(34,'ESPN MEXICO','permanente',2),(35,'CLARO SPORTS','permanente',2),(36,'ESPN PLAY','permanente',2),(37,'ABC CWPO-TV Cincinnati','ocasional',2),(38,'ALTITUDE','ocasional',2),(39,'ALTITUDE ALT','ocasional',2),(40,'ANTENA TRES','ocasional',2),(41,'BIG 10 Network','ocasional',2),(42,'FOX-NFL','ocasional',2),(43,'FOX DEPORTES','ocasional',2),(44,'FOX SPORTS ARIZONA','ocasional',2),(45,'FOX SPORTS CAROLINA','ocasional',2),(46,'FOX SPORTS Cincinnati','ocasional',2),(47,'FOX SPORTS Detroit','ocasional',2),(48,'FOX SPORTS Florida','ocasional',2),(49,'FOX SPORTS Indiana','ocasional',2),(50,'FOX SPORTS Milwaukee','ocasional',2),(51,'FOX SPORTS New Orleans','ocasional',2),(52,'FOX SPORTS North','ocasional',2),(53,'FOX SPORTS Ohio','ocasional',2),(54,'FOX SPORTS Oklahoma','ocasional',2),(55,'FOX SPORTS San Diego','ocasional',2),(56,'FOX SPORTS South','ocasional',2),(57,'FOX SPORTS South East','ocasional',2),(58,'FOX SPORTS South West','ocasional',2),(59,'FOX SPORTS SUN','ocasional',2),(60,'FOX SPORTS Tennessee','ocasional',2),(61,'FOX SPORTS West','ocasional',2),(62,'FOX SPORTS Wisconsin','ocasional',2),(63,'FS1','ocasional',2),(64,'FS2','ocasional',2),(65,'GALAVISION','ocasional',2),(66,'GALICIA SD','ocasional',2),(67,'MASN','ocasional',2),(68,'NBC SPORT NETWORK','ocasional',2),(69,'NESN','ocasional',2),(70,'NHL NET','ocasional',2),(71,'RAI','ocasional',2),(72,'ROOT NW','ocasional',2),(73,'TNT','ocasional',2),(74,'TVE SD','ocasional',2),(75,'UNIMAS','ocasional',2),(76,'UNIVISION','ocasional',2),(77,'YES','ocasional',2),(78,'STO-SPORT TIME OHIO','ocasional',2),(79,'IMAGEN','ocasional',2),(80,'ESPN+','ocasional',2),(81,'TVC','ocasional',2),(82,'CBS SPORTS NETWORK','ocasional',2),(83,'SKY-LOCAL','ocasional',2),(84,'TDN-LOCAL','ocasional',2),(85,'TVSA-LOCAL','ocasional',2);
/*!40000 ALTER TABLE `canal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `cargar_origen` int(11) DEFAULT NULL,
  `cargar_canales` int(11) DEFAULT NULL,
  `cargar_via` int(11) DEFAULT NULL,
  `description` text,
  `color` varchar(255) DEFAULT NULL,
  `textColor` varchar(255) NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (3,'WSW',1,2,30,'WFWEF','#307757','#FFFFFF','2019-05-01 21:00:00','2019-05-02 22:00:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origen`
--

DROP TABLE IF EXISTS `origen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origen` (
  `origen_id` int(11) NOT NULL AUTO_INCREMENT,
  `origen_name` varchar(250) NOT NULL,
  PRIMARY KEY (`origen_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origen`
--

LOCK TABLES `origen` WRITE;
/*!40000 ALTER TABLE `origen` DISABLE KEYS */;
INSERT INTO `origen` VALUES (1,'Miami'),(2,'Mexico');
/*!40000 ALTER TABLE `origen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static`
--

DROP TABLE IF EXISTS `static`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_title` varchar(60) DEFAULT NULL,
  `s_color` varchar(255) DEFAULT NULL,
  `s_textcolor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static`
--

LOCK TABLES `static` WRITE;
/*!40000 ALTER TABLE `static` DISABLE KEYS */;
INSERT INTO `static` VALUES (47,'EVENTO X','rgb(255, 133, 27)','#FFFFFF'),(49,'VENTE','rgb(60, 141, 188)','#FFFFFF'),(50,'VENTE','rgb(57, 204, 204)','#FFFFFF');
/*!40000 ALTER TABLE `static` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` VALUES (1,'Administrador'),(2,'Usuario');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(30) NOT NULL,
  `password` varchar(130) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(80) NOT NULL,
  `last_session` datetime DEFAULT NULL,
  `activacion` int(11) NOT NULL DEFAULT '0',
  `token` varchar(40) NOT NULL,
  `token_password` varchar(100) DEFAULT NULL,
  `password_request` int(11) DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `via`
--

DROP TABLE IF EXISTS `via`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `via` (
  `via_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria',
  `via_nombre` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`via_id`),
  KEY `nombre` (`via_nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1 COMMENT='tabla de vias';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `via`
--

LOCK TABLES `via` WRITE;
/*!40000 ALTER TABLE `via` DISABLE KEYS */;
INSERT INTO `via` VALUES (28,'ESTUDIO A'),(9,'ESTUDIO C'),(23,'ESTUDIO D'),(8,'ESTUDIO E'),(21,'ESTUDIO I'),(11,'ESTUDIO J'),(20,'FIBRA'),(26,'MASTER MTY'),(3,'NEWS CENTER'),(30,'NOTICIERO'),(4,'PARABOLA'),(24,'PARABOLA NIMBRA'),(29,'SKY 1102'),(5,'SKY 1103'),(27,'SKY 1501'),(13,'SKY 1516'),(15,'SKY 1517'),(14,'SKY 1518'),(17,'SKY 1524'),(18,'SKY 1526'),(16,'SKY 1527'),(10,'SKY 1553'),(25,'SKY 1556'),(2,'SKY 271'),(19,'SKY 272'),(6,'SKY 273'),(22,'SKY 501'),(12,'SKY 548'),(1,'SKY 551'),(7,'SKY 563');
/*!40000 ALTER TABLE `via` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'sistema'
--

--
-- Dumping routines for database 'sistema'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-06 18:18:01
