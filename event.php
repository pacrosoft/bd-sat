<?php

header('Content-Type: application/json');

$pdo= new PDO("mysql:dbname=sistema;host=localhost","pacrosoft","M4rc14n4.27@");

$accion=(isset($_GET['accion']))?$_GET['accion']:'leer';

switch ($accion) {
	case 'agregar':
		//agregar las instrucciones
		$sentenciaSQL=$pdo->prepare("INSERT INTO
			events(title,cargar_origen,cargar_canales,cargar_via,description,color,textColor,start,end)
			values(:title,:cargar_origen,:cargar_canales,:cargar_via,:description,:color,:textColor,:start,:end)");

			$respuesta=$sentenciaSQL->execute(array(
				"title" =>$_POST['title'],
				"cargar_origen" =>$_POST['cargar_origen'],
				"cargar_canales" =>$_POST['cargar_canales'],
				"cargar_via" =>$_POST['cargar_via'],
				"description" =>$_POST['description'],
				"color" =>$_POST['color'],
				"textColor" =>$_POST['textColor'],
				"start" =>$_POST['start'],
				"end" =>$_POST['end']
			));

			echo json_encode($respuesta);
		break;

	case 'agregarDefault':
		//agregar las instrucciones
		$sentenciaSQL=$pdo->prepare("INSERT INTO
			events(title,cargar_origen,cargar_canales,cargar_via,description,color,textColor,start,end)
			values(:title,:cargar_origen,:cargar_canales,:cargar_via,:description,:color,:textColor,:start,:end)");

			$respuesta=$sentenciaSQL->execute(array(
				"title" =>$_POST['title'],
				"cargar_origen" =>$_POST['cargar_origen'],
				"cargar_canales" =>$_POST['cargar_canales'],
				"cargar_via" =>$_POST['cargar_via'],
				"description" =>$_POST['description'],
				"color" =>$_POST['color'],
				"textColor" =>$_POST['textColor'],
				"start" =>$_POST['start'],
				"end" =>$_POST['end']
			));

			echo json_encode($respuesta);
		break;

	case 'addstatic':
		//agregar las instrucciones
		$sentenciaSQL=$pdo->prepare("INSERT INTO
			static(s_title,s_color,s_textcolor)
			values(:s_title,:s_color,:s_textcolor)");

			$respuesta=$sentenciaSQL->execute(array(
				"s_title" =>$_POST['s_title'],
				"s_color" =>$_POST['s_color'],
				"s_textcolor" =>$_POST['s_textcolor']
			));

			echo json_encode($respuesta);
		break;

	case 'eliminar':
		//borrar instrucciones
		$respuesta=false;
		if(isset($_POST['id'])){
			$sentenciaSQL=$pdo->prepare("DELETE FROM events WHERE id=:id");
			$respuesta=$sentenciaSQL->execute(array(
				"id" =>$_POST['id']
			));

		}
		echo json_encode($respuesta);
		break;
	case 'actualizar':
		//actualiza  instrucciones
		$sql="UPDATE events SET title=:title,description=:description,cargar_origen=:cargar_origen,cargar_canales=:cargar_canales,cargar_via=:cargar_via,color=:color,textColor=:textColor,start=:start,end=:end WHERE id=:id";
		
		$sentenciaSQL=$pdo->prepare($sql);
		$respuesta=$sentenciaSQL->execute(array(
				"id" =>$_REQUEST['id'],
				"title" =>$_REQUEST['title'],
				"description" =>$_REQUEST['description'],
				"cargar_origen" =>$_REQUEST['cargar_origen'],
				"cargar_canales" =>$_REQUEST['cargar_canales'],
				"cargar_via" =>$_REQUEST['cargar_via'],
				"color" =>$_REQUEST['color'],
				"textColor" =>$_REQUEST['textColor'],
				"start" =>$_REQUEST['start'],
				"end" =>$_REQUEST['end']
		));
		echo json_encode($respuesta);
		break;
		default:
		//selecciona los eventos del booking
		$sentenciaSQL=$pdo->prepare("SELECT * from events");
		$sentenciaSQL->execute();
		$resultado= $sentenciaSQL->fetchAll(PDO::FETCH_ASSOC);

		echo json_encode($resultado);
		break;
}

?>
