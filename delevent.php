<?php
include "autoload.php";
include "controller/StaticData.php";
?>
<?php 
if (isset($_GET['s_id']))//codigo elimina un elemento del array
{
	
	$del = StaticData::getById($_GET["s_id"]);
	$del->del();

};

?>

<?php $statics = StaticData::getAll();
    if(count($statics)>0){ ?>
    <?php foreach($statics as $static):?> 
    <div  class="external-event" style="background-color:<?php echo $static->s_color; ?>;color:white;"><?php echo $static->s_title; ?><a href="#" style="color: white;" onclick="eliminar('<?php echo $static->s_id; ?>')" class="pull-right"><i class="glyphicon glyphicon-trash"></i> </a></div>

     <?php endforeach; ?>
<?php }else{ }; ?>

<script>

  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 1070,
          revert: true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        });

      });
    }

    ini_events($('#external-events div.external-event'));
  });
</script>